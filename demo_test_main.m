%% Clear/close all
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
clear all; bdclose all;

%% Expcode setup
fprintf('\n### EXPCODE SETUP ###\n');
obj=expcodeconf_node06_demo12_test;
obj.init;
obj.setup;
%% default tunable parameters data upload to MDS for algorithm demo1
fprintf('\n### ALGO1 AND ALGO2 DEFAULT TP UPLOAD TO MDSPLUS ###\n');
% TODO, this should be feasible directly from main expcode object
% by calling obj.updatemdsmodel('algo_demo1') etc.
% now it is failing due to references upload failure
demo1=obj.algos{1};
demo1.updateparameters(-1);
demo2=obj.algos{2};
demo2.updateparameters(-1);

%% expcode actualize
fprintf('\n### EXPCODE ACTUALIZE ###\n');
obj.actualize(-1);

%% expcode simulate
fprintf('\n### EXPCODE SIMULATION ###\n');
%obj.sim;
sim(obj.mainslxname);

%% wrappers build
fprintf('\n### CODE GENERATION ###\n');
obj.build; 

%% MARTe2 cfg file autogeneration
fprintf('\n### MARTE2 CFG FILE GENERATION ###\n');
cd nodes/marte2cfg/2threads/
algo_demo1and2_create_marte2_cfg;
cd ../../..

%% Transfer gencode and cfg to the realtime machine
fprintf('\n### FILE COPY TO martecfgs/tests ###\n');
%cd(fileparts(mfilename('fullpath'))); % this goes into /tmp/Editor on scd.epfl.ch
if isequal(SCDDSdemo_env.getsite(),'EPFL')
system('cp nodes/marte2cfg/2threads/algo_demo1and2_2threads.cfg ~/martecfgs/tests');
system('cp gencodes/CodeGenFolder-1/SCDwrap_demo1.so ~/martecfgs/tests');
system('cp gencodes/CodeGenFolder-1/SCDwrap_demo2.so ~/martecfgs/tests');
elseif isequal(SCDDSdemo_env.getsite(),'PSFC')
system('cp nodes/marte2cfg/2threads/algo_demo1and2_2threads.cfg    ~/MARTe2/MARTe2-examples/Configurations');
system('cp gencodes/CodeGenFolder-1/SCDwrap_demo1.so      ~/MARTe2/MARTe2-examples');
system('cp gencodes/CodeGenFolder-1/SCDwrap_demo2.so      ~/MARTe2/MARTe2-examples');
end

% MARTe2 run via scdstarter

switch SCDDSdemo_env.getsite()
    case 'EPFL'
    fprintf('\n### MARTE2 RUN ON LOCALHOST (follow it on spcscddev logs) ###\n');
    system('echo -n "Class = Message Destination = ShotStarter Function = INIT" | nc spcscddev 24681');
    case 'PSFC'
    fprintf('\n### MARTE2 RUN ON LOCALHOST ###\n');
    system('export MARTe2_Components_DIR=~/MARTe2/MARTe2-components; source /etc/profile.d/mdsplus.sh; cd ~/MARTe2/MARTe2-examples; ./runcfs.sh'); 
end

% poor man way of waiting MARTe2 job done
% MARTe2 is launched with a timeout of 10 seconds
% fprintf('\n### PAUSING 15 seconds ###\n');
% pause(15); 

%% MARTe2 output retrieval
% disconnect from mds in case already opened connections.
mdsdisconnect();

fprintf('\n### MARTE2 DATA RETRIEVAL FROM MDSPLUS ###\n');
switch SCDDSdemo_env.getsite()
    case 'EPFL'
        mdsconnect('spcscddev');
    case 'PSFC'
        mdsconnect('alcdaq6.psfc.mit.edu');
end

marteshot=mdsopen('scdds',0);

demo1signal1marte2=tdi('demo1.outputs.demo1.signal1');
demo1signal2marte2=tdi('demo1.outputs.demo1.signal2');
demo1signal3marte2=tdi('demo1.outputs.demo1.signal3');
demo1signal4marte2=tdi('demo1.outputs.demo1.signal4');
demo1signal5marte2=tdi('demo1.outputs.demo1.signal5');
demo1signal6marte2=tdi('demo1.outputs.demo1.signal6');
demo1signal7marte2=tdi('demo1.outputs.demo1.signal7');
demo1signal8marte2=tdi('demo1.outputs.demo1.signal8');
demo1signal9marte2=tdi('demo1.outputs.demo1.signal9');

demo2signal1marte2=tdi('demo2.outputs.demo2.signal1');
demo2signal2marte2=tdi('demo2.outputs.demo2.signal2');
demo2signal3marte2=tdi('demo2.outputs.demo2.signal3');
demo2signal4marte2=tdi('demo2.outputs.demo2.signal4');

demo1threadcycletime  =tdi('demo1.system.ch01');
demo1simulinkreadtime =tdi('demo1.system.ch02');
demo1simulinkexectime =tdi('demo1.system.ch03');
demo1simulinkwritetime=tdi('demo1.system.ch04');

demo2threadcycletime  =tdi('demo2.system.ch01');
demo2simulinkreadtime =tdi('demo2.system.ch02');
demo2simulinkexectime =tdi('demo2.system.ch03');
demo2simulinkwritetime=tdi('demo2.system.ch04');

%% Plot combined simulation / realtime ouput signals
fprintf('\n### SIMULINK SIM / MARTE2 EXEC COMBINED PLOT ###\n');
figure(1);
clf
hold on

%simline='k--';
simline='k+';

plot(demo1simout.demo1.signal1,simline);
plot(demo1simout.demo1.signal2,simline);
plot(demo1simout.demo1.signal4,simline);
plot(demo2simout.demo2.signal1,simline);
plot(demo2simout.demo2.signal2,simline);
plot(demo2simout.demo2.signal4,simline);

%marteline='r-.';
marteline='ro';

plot(demo1signal1marte2,marteline);
plot(demo1signal2marte2,marteline);
plot(demo1signal4marte2,marteline);
plot(demo2signal1marte2,marteline);
plot(demo2signal2marte2,marteline);
plot(demo2signal4marte2,marteline);

xlim([0 3]);
xlabel('time [s]');
grid on

title('black cross: Simulink simulation, red circles MARTe2 run')

%% Plot timings
figure(2)
plot(demo1threadcycletime,'k');
hold on
plot(demo2threadcycletime,'r');

plot(demo1simulinkreadtime,'g');
plot(demo1simulinkexectime,'g');
plot(demo1simulinkwritetime,'g');
plot(demo2simulinkreadtime,'m');
plot(demo2simulinkexectime,'m');
plot(demo2simulinkwritetime,'m');

grid on
ylabel('[us]');
xlabel('time [s]');
title('MARTe2 timings');
%ylim([800 1200]);

title('k: T1 cyclet, r: T2 cyclet, g: demo1 execs, m: demo2 execs');

%% Plot vector signals
figure(3);
clf;
hold on;

% vector signals ordering shoud not change on MDSplus, hence:
plot(demo1simout.demo1.signal6.Time, demo1simout.demo1.signal6.Data(:,1),'r');
plot(demo1simout.demo1.signal6.Time, demo1simout.demo1.signal6.Data(:,2),'g');
plot(demo1simout.demo1.signal8.Time, demo1simout.demo1.signal8.Data(:,1),'r');
plot(demo1simout.demo1.signal8.Time, demo1simout.demo1.signal8.Data(:,2),'g');
chkoffst=0.1;
plot(demo1signal6marte2.dim{1},demo1signal6marte2.data(1,:)+chkoffst,'r');
plot(demo1signal6marte2.dim{1},demo1signal6marte2.data(2,:)+chkoffst,'g');
plot(demo1signal8marte2.dim{1},demo1signal8marte2.data(1,:)+chkoffst,'r');
plot(demo1signal8marte2.dim{1},demo1signal8marte2.data(2,:)+chkoffst,'g');

xlim([0 3]);
title('vector signals comparison');

%% Plot matrix signals
figure(4);
clf;
hold on;

% matrix signals should be stored as row-major encoded vectors on MDSplus,
% hence:
matsig=demo1simout.demo1.signal7;
plot(matsig.Time,reshape(matsig.Data(1,1,:),1,size(matsig.Data(1,1,:),3)'),'r');
plot(matsig.Time,reshape(matsig.Data(1,2,:),1,size(matsig.Data(1,2,:),3)'),'g');
plot(matsig.Time,reshape(matsig.Data(1,3,:),1,size(matsig.Data(1,3,:),3)'),'b');
plot(matsig.Time,reshape(matsig.Data(2,1,:),1,size(matsig.Data(2,1,:),3)'),'k');
plot(matsig.Time,reshape(matsig.Data(2,2,:),1,size(matsig.Data(2,2,:),3)'),'m');
plot(matsig.Time,reshape(matsig.Data(2,3,:),1,size(matsig.Data(2,3,:),3)'),'c');
matsig=demo1simout.demo1.signal9;
plot(matsig.Time,reshape(matsig.Data(1,1,:),1,size(matsig.Data(1,1,:),3)'),'r');
plot(matsig.Time,reshape(matsig.Data(1,2,:),1,size(matsig.Data(1,2,:),3)'),'g');
plot(matsig.Time,reshape(matsig.Data(1,3,:),1,size(matsig.Data(1,3,:),3)'),'b');
plot(matsig.Time,reshape(matsig.Data(2,1,:),1,size(matsig.Data(2,1,:),3)'),'k');
plot(matsig.Time,reshape(matsig.Data(2,2,:),1,size(matsig.Data(2,2,:),3)'),'m');
plot(matsig.Time,reshape(matsig.Data(2,3,:),1,size(matsig.Data(2,3,:),3)'),'c');
chkoffst=0.1;
plot(demo1signal7marte2.dim{1},demo1signal7marte2.data(1,:)+chkoffst,'r');
plot(demo1signal7marte2.dim{1},demo1signal7marte2.data(2,:)+chkoffst,'g');
plot(demo1signal7marte2.dim{1},demo1signal7marte2.data(3,:)+chkoffst,'b');
plot(demo1signal7marte2.dim{1},demo1signal7marte2.data(4,:)+chkoffst,'k');
plot(demo1signal7marte2.dim{1},demo1signal7marte2.data(5,:)+chkoffst,'m');
plot(demo1signal7marte2.dim{1},demo1signal7marte2.data(6,:)+chkoffst,'c');
plot(demo1signal9marte2.dim{1},demo1signal9marte2.data(1,:)+chkoffst,'r');
plot(demo1signal9marte2.dim{1},demo1signal9marte2.data(2,:)+chkoffst,'g');
plot(demo1signal9marte2.dim{1},demo1signal9marte2.data(3,:)+chkoffst,'b');
plot(demo1signal9marte2.dim{1},demo1signal9marte2.data(4,:)+chkoffst,'k');
plot(demo1signal9marte2.dim{1},demo1signal9marte2.data(5,:)+chkoffst,'m');
plot(demo1signal9marte2.dim{1},demo1signal9marte2.data(6,:)+chkoffst,'c');

xlim([0 3]);
title('matrix signals comparison');
