classdef SCDDSdemo_node < SCDDSclass_node
  % Superclass or SCDDSdemo_node
  %
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  methods
    function node = defaultnodeconfig(node,nodenr)
      node.name = sprintf('NODE%02d',nodenr); %Default node names
      switch nodenr
        case 6
          node.ncpu = 2;
          node.type = '4cpus2015a';
          node.timing.t_start = 0.0;
          node.timing.t_stop = 2.75;
          node.timing.dt = 1e-3;
          node.buildcfg.conffile = cell(node.ncpu,1);
          node.buildcfg.conffile{1} = 'standard';
          node.buildcfg.conffile{2} = 'standard';
          node.buildcfg.initscdbeforecomp = [0 0];
	  node.buildcfg.confset{1} = 'configurationSettingsCODEgcc';
	  node.buildcfg.confset{2} = 'configurationSettingsCODEgcc';
          node.haswavegen = true;
          node.hasadc = false;
        otherwise
          error('node not defined')
      end
    end
  end
end
