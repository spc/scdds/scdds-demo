classdef  SCDDSdemo_expcode < SCDDSclass_expcode
  properties (SetAccess = protected)
    mainname        = 'demomain'   % Main slx moded name
    definednodes    = 6;       % defined node
  end
  
  methods
    function obj=SCDDSdemo_expcode(varargin)
      % Constructor for SCDclass_expcode
      % Call superclass constructor
      %
      %
      % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
      % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
      obj@SCDDSclass_expcode(varargin{:});

      % override algonameprefix
      obj.algonameprefix  = 'algo'; % Algorithm name prefix
    end
    
    function [ok]=build(obj)
      % set specific configuration settings 
      SCDconf_setConf('configurationSettingsCODEgcc');
      % build
      ok = build@SCDDSclass_expcode(obj); % call superclass method
    end
    
    function node = getdefaultnode(~,~)
      % Default node to populate empty nodes if any
      node = SCDDSdemo_node.empty;
    end
  end
end
