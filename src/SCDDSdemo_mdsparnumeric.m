classdef SCDDSdemo_mdsparnumeric < SCDDSclass_mdsparnumeric
  % Parameter interface class
  %
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  methods
    function obj=SCDDSdemo_mdsparnumeric(mdssrcnode,destparam,varargin)
      mdsconfig = SCDDSdemo_mdsconfig();
           
      % call constructor
      obj@SCDDSclass_mdsparnumeric(mdssrcnode,destparam,...
        'srcsrv',mdsconfig.mdsserver,...
        'srctree',mdsconfig.mdstree,...
        'mdsvalid','1',...
        varargin{:});    
      
      % set MDS client type
      obj.clienttype="Thin";
      %obj.clienttype="Distributed";      
      
      % set load permissivity
      obj.skipnotok=true;
      
      % set data orientation
      %obj.dataorientation="RowMajor";
      
      % set MDS interface
      obj.MDSinterface = mdsconfig.mdsinterface;
    end
  end
end