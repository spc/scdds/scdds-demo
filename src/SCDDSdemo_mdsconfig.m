classdef SCDDSdemo_mdsconfig  < SCDDSclass_mdsconfig
  % SCDDSdemo_mdsconfig
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  methods
    function obj = SCDDSdemo_mdsconfig()
      % constructor
      
      % with this approach the user inherits
      % MDS settings from SCDDScore:
      whereami = SCDDSdemo_env.getsite;
      obj@SCDDSclass_mdsconfig(whereami);
      
      % alternatively the user can define its own
      % specific MDS settings here (possibly 
      % with environment checks routines) in this way:
      % 
      % obj.mdsserver    = 'myserver';       % default server name
      % obj.mdstree      = 'mytree';         % default tree used
      % obj.mdsinterface = MDSplus_java;     % used MDSplus interface class 
      %
      % but the user is warned that in this case mds related
      % core tests may fail in his/her environment.
      %
      % As a third alternative, the user can modify directly the
      % core SCDDSclass_mdsconfig class to add his/her specific MDS
      % settings, and leave the automatic inheritance core here unchanged
    end
  end
end