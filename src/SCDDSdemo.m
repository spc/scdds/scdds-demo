classdef SCDDSdemo < SCDDSclass
  % Class for testing basic SCDDSdemo aspects
  %
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  methods(Static)
    function setup_paths
      rootpath = fileparts(fileparts(mfilename('fullpath')));
      run(fullfile(rootpath,'scdds_demo_paths'));
    end
  end
end