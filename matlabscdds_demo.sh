#!/bin/bash
COREPATH=core
export MARTe2_DIR=~/MARTe2/MARTe2-dev
USERARG=$@
if [[ -z "$USERARG" ]]; then USERARG="scdds_demo_paths;"; fi
cmd="$COREPATH/matlabscdds.sh $USERARG"
echo $cmd
$cmd
