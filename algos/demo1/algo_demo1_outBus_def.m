% Bus object: SCDalgo_demo1_outBus 
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'signal1';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
%elems(1).DataType = 'Bus:SCDBus_single_11_Vector';
elems(1).DataType = 'single';
elems(1).SampleTime = -1;
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'signal2';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
%elems(2).DataType = 'Bus:SCDBus_int32_Scalar';
elems(2).DataType = 'single';
elems(2).SampleTime = -1;
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

elems(3) = Simulink.BusElement;
elems(3).Name = 'signal3';
elems(3).Dimensions = 1;
elems(3).DimensionsMode = 'Fixed';
elems(3).DataType = 'single';
elems(3).SampleTime = -1;
elems(3).Complexity = 'real';
elems(3).Min = [];
elems(3).Max = [];
elems(3).DocUnits = '';
elems(3).Description = '';

ii=4;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal4';
elems(ii).Dimensions = 1;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=5;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal5';
elems(ii).Dimensions = 1;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=6;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal6';
elems(ii).Dimensions = 2;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=7;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal7';
elems(ii).Dimensions = [2 3];
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=8;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal8';
elems(ii).Dimensions = 2;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=9;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal9';
elems(ii).Dimensions = [2 3];
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

algo_demo1_outBus = Simulink.Bus;
algo_demo1_outBus.HeaderFile = '';
algo_demo1_outBus.Description = '';
algo_demo1_outBus.DataScope = 'Auto';
algo_demo1_outBus.Alignment = -1;
algo_demo1_outBus.Elements = elems;
clear elems;
