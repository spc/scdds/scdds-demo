function TP = algo_demo1_loadtp(obj)
% Setup tunable control params default values
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

TP.enable           = true;
TP.gain             = single(2);
TP.refmodel1        = algo_demo1_referenced_loadtp(); % default copy of tp substructure for referenced subsystem 1
TP.refmodel2        = algo_demo1_referenced_loadtp(); % default copy of tp substructure for referenced subsystem 2
TP.rowvect          = int32([1 2 3]);
TP.colvect          = single([1 2 1 pi/4 5]');
TP.matrix           = int16([11 12 13 14; 21 22 23 24; 31 32 33 34; 41 42 43 44]);
TP.fixdimvector1    = single([1 2 3 4]);
TP.fixdimvector2    = int32([1 2 3 4]);
TP.vectindex        = int8([1 2]);
TP.rmatrix          = int16([11 12 13 14; 21 22 23 24; 31 32 33 34]);
TP.tdmatrix         = int16(zeros(2,3,4));
TP.tdmatrix(:,:,1)  = int16(1*[111 121 131; 211 221 231]);
TP.tdmatrix(:,:,2)  = int16(1*[112 122 132; 212 222 232]);
TP.tdmatrix(:,:,3)  = int16(1*[113 123 133; 213 221 233]);
TP.tdmatrix(:,:,4)  = int16(1*[114 124 134; 214 224 234]);

end
