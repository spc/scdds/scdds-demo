classdef algo_demo1_test < algo_test
  % test class for algo_demo1
  %
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  properties
    algoobj = @algoobj_demo1;
  end
end