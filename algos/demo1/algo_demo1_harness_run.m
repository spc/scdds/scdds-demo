function algo_demo1_harness_run(obj)
% run harness and check the result
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

% load tunable control parameters from mds
shot = -1; % shot reference
obj.actualize(shot);

% Input Data - must be structure of timeseries matching input bus structure
% time = 0:obj.gettiming.dt:1;
% data = single(sin(2*pi*time*10)');
% % NB: these struct name and types must match the corresponding data bus
% tsStructData = struct('simple_signal',timeseries(data,time,'Name','Test Input Data'));

% SimIn object to customize configuration of Simulation run for test
SimIn = Simulink.SimulationInput([obj.getname '_harness']);

% Create demo1 Input dataset for this model
% Dataset = createInputDataset(obj.getname);

% assign input data signal to Simulink.Signal object
% DataIn = Simulink.SimulationData.Signal;
% DataIn.Values = tsStructData;

% assign as first element in input data set
% isig = find(contains(Dataset.getElementNames,'signal_in')); % find input signal index
% Dataset = Dataset.setElement(isig,DataIn); %#ok<FNDSB>
% SimIn.ExternalInput = Dataset; % assign this as external input for simulation

% Custom parameters for this run to save outport data
SimIn = SimIn.setModelParameter('SaveOutput','on'); % set to save outport signals
SimIn = SimIn.setModelParameter('SignalLogging','on'); % set to save log signals
SimIn = SimIn.setModelParameter('OutputSaveName','SimOut');
SimIn = SimIn.setModelParameter('SaveFormat','Dataset');
SimIn = SimIn.setModelParameter('StartTime',num2str(obj.gettiming().t_start));
SimIn = SimIn.setModelParameter('StopTime',num2str(obj.gettiming().t_stop));
SimIn = SimIn.setModelParameter('FixedStep',num2str(obj.gettiming().dt));

% simulate - simulate only single types to save time
result = sim(SimIn);

% check output port data
tp = Simulink.data.evalinGlobal(obj.getname,'algo_demo1_tp.Value');
fp = Simulink.data.evalinGlobal(obj.getname,'algo_demo1_fp');

%input = evalin('base','algo_demo1_inbus1');
input = evalin('base','algo_demo1_simdata');
input1  = input.signal1.Data;
input2  = input.signal2.Data;

output1 = result.simout.signal1.Data;
output2 = result.simout.signal2.Data;

size(input1)
size(output1)

maxerror1 = 0.1;
maxerror2 = 0.1; 
assert(max(abs(output1 - input1.*tp.gain))<maxerror1,'Wrong output 1!');
assert(max(abs(output2 - (input2.*tp.refmodel1.gain+fp.refmodel1.offset)))<maxerror2,'Wrong output 2!');

% check logs which contain a SCDsignal type data
% logsout = result.logsout;
% 
% signal    = logsout.getElement('SCDsignal type bus').Values.Value;
% Quality   = logsout.getElement('SCDsignal type bus').Values.QualityTag;
% State     = logsout.getElement('SCDsignal type bus').Values.ProductionState;
% 
% assert(all(State  .Data == ProductionState.RUNNING),'ProductionState must be RUNNING');
% assert(all(Quality.Data == QualityTag.GOOD        ),'QualityTag must be RUNNING'     );
end
