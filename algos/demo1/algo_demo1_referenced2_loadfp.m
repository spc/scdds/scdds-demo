function fp = algo_demo1_referenced2_loadfp()

%% Load other fixed parameters
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
fp.offset = 5; % a fixed parameter to be passed to referenced subsystem 1
fp.foo    = single(2);
end