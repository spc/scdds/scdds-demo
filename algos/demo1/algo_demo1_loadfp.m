function fp = algo_demo1_loadfp(obj)

%% Load other fixed parameters
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
fp.timing = obj.gettiming;
fp.number=single(1);  % a fixed parameter
% referenced subsystems fixed parameters structures
% note that the two following function can return
% different fixed parameters values but their overall
% structure must be identical
% (topologically and in data types)
fp.refmodel1=algo_demo1_referenced1_loadfp(); % fp for referenced subsystem 1
fp.refmodel2=algo_demo1_referenced2_loadfp(); % fp for referenced subsystem 2

end