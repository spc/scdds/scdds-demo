function [busNames,Buses] = algo_demo1_signal_buses()
% [busNames,Buses] = SCDalgo_demo1_signal_buses()
% Define some buses needed for the demo1
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

% BELOW ARE SOME STRUCTURED BUSES WHICH ARE NOT COMPATIBLE NOW
% BUT WOULD BE NICE TO REVIVE
% %% Define signal buses for various SCDsignal types
% % Data examples for which to create buses
% D = {...
%   ones(11,1,'single'),...
%   ones(3,3,'single'),...
%   int32(1)};
% 
% % Create buses for these signals and add them to the list
% [Buses,busNames] = deal(cell(numel(D),1)); % init
% for ii = 1:numel(D)
%   mydata = D{ii};
%   mysig = SCDsignal(mydata); % create SCDsignal object for this data example
%   mybus = mysig.createBus; % create Bus for this signal using object method
%   % Store
%   Buses{ii} = mybus;
%   busNames{ii} = mybus.Description;
% end
% 
% %% Add output structure: a bus that includes only SCDsignal data types
% 
% % First add 3 busElements for the three signals defined above
% for ii=1:3
% elems(ii) = Simulink.BusElement;
% elems(ii).Name = sprintf('signal%d',ii);
% elems(ii).Dimensions = 1;
% elems(ii).DimensionsMode = 'Fixed';
% elems(ii).DataType = sprintf('Bus: %s',busNames{ii});
% end
% 
% % Define output bus with these elements
% outBus = Simulink.Bus;
% outBus.HeaderFile = '';
% outBus.Description = '';
% outBus.DataScope = 'Auto';
% outBus.Alignment = -1;
% outBus.Elements = elems;
% clear elems;
% 
% % append to list
% busNames{end+1} = 'algo_demo1_bus1';
% Buses{end+1}    = outBus; 

busNames={};
Buses={};

% init bus
elems(1)=Simulink.BusElement;
elems(1).Name='clipgain1';
elems(1).DataType='single';
elems(2)=Simulink.BusElement;
elems(2).Name='tstart1';
elems(2).DataType='single';
elems(3)=Simulink.BusElement;
elems(3).Name='tstop1';
elems(3).DataType='single';
elems(4)=Simulink.BusElement;
elems(4).Name='enable1';
elems(4).DataType='boolean';
elems(5)=Simulink.BusElement;
elems(5).Name='freq';
elems(5).DataType='single';
elems(6)=Simulink.BusElement;
elems(6).Name='phase';
elems(6).DataType='single';


initBus = Simulink.Bus;
initBus.HeaderFile = '';
initBus.Description = '';
initBus.DataScope = 'Auto';
initBus.Alignment = -1;
initBus.Elements = elems;
clear elems;

% append to list
busNames{end+1} = 'algo_demo1_init1';
Buses{end+1}    = initBus; 


end
