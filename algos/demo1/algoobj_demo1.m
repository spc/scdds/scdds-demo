function obj = algoobj_demo1()

% Demonstration algorithm 1
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
obj = SCDDSdemo_algo('algo_demo1');

%% Timing of the algorithm
obj=obj.settiming(-1,1e-3,1.0);

%% Fixed parameters init functions 
% Regular fixed parameters structure
obj=obj.addfpinitfcn('algo_demo1_loadfp','algo_demo1_fp');
% Copy of tunable parameters structure inserted as fixed 
% used to check compile time vs. runtime values of tunable
% parameters 
obj=obj.addfpinitfcn('algo_demo1_loadtp','algo_demo1_tp_build');

%% Tunable parameters structure name
% Regular tunable parameters structure
obj=obj.addtunparamstruct('algo_demo1_tp', @()algo_demo1_loadtp(), false);

%% Tunable parameters
% Possible bug with MARTe2 distributed client, see scdds-demo project issue #5
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.enable'        ,'enable'          ,'mdshelp','a bool param')); 
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.gain'          ,'gain'            ,'mdshelp','a tunable gain'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.refmodel1.gain','refmodel1.gain'  ,'mdshelp','a tunable gain for a referenced subsystem'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.refmodel2.gain','refmodel2.gain'  ,'mdshelp','a tunable gain for another referenced subsystem'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.rowvect'       ,'rowvect'         ,'mdshelp','a row vector'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.colvect'       ,'colvect'         ,'mdshelp','a column vector'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.matrix'        ,'matrix'          ,'mdshelp','a square matrix'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.fixdivector1'  ,'fixdimvector1'   ,'mdshelp','a fixed dimension vector','dim',4));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.fixdivector2'  ,'fixdimvector2'   ,'mdshelp','a fixed dimension vector','dim',4));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.vectindex%02d' ,'vectindex'       ,'mdshelp','a wildchard subst loading','startidx',1,'stopidx',2));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.rmatrix'       ,'rmatrix'         ,'mdshelp','a rectangular matrix'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo1.params.tdmatrix'      ,'tdmatrix'        ,'mdshelp','a three dimensional matrix'));

%% Wavegens
mdsconfig = SCDDSdemo_mdsconfig();
obj=obj.addwavegenbasetruct('algo_demo1_inbus1');
obj=obj.addwavegen(SCDclass_mdswgsigsingle( 'demo1.inputs.signal1','signal1'        ,'srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree));
obj=obj.addwavegen(SCDclass_mdswgsigsingle( 'demo1.inputs.signal2','signal2'        ,'srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree));

%% Buses
obj = obj.addbus('algo_demo1_inBus', 'algo_demo1_inBus_def' );
obj = obj.addbus('algo_demo1_outBus', 'algo_demo1_outBus_def' );

% function handle that returns cell arays of buses and busnames to be registered
obj = obj.addbus('',@() algo_demo1_signal_buses());

% buses can be defined also starting from a matlab structurem handy in case
% of tunable parameters structures, as it defined automatically
% a bus to corry them around the model
obj = obj.addbus('algo_demo1_tp_bus',@() algo_demo1_loadtp());

%% Tasks

%% Print (optional)
obj.printinfo;

end

