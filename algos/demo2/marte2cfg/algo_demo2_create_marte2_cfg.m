% This script creates a testable
% MARTe2 cfg file for the demo2 algorithm
% with these specifications:
%
% tunable parameters MDSplus link via MDSObjLoader instatiation
% tunable waveforms  MDSplus link via MDSObjWavegen instantiation
% single realtime thread, LinuxTimer synched at 1kHz
% algorithms output MDSplus link via standard MDSWriter
% 
% it is assumend that a demo2 algorithm decritpion object
% has already been siccessfully defined, inited and setup, via:
% obj=algo_demo2; obj.init; obj.setup;
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

%% Parameters
% source MDSplus shotnumber
mdssrcshot=-1;
% destination MDSplus shotnumber (-1 stands for increment current by 1)
mdsdstshot=-1;
% verbosity level
verbosity=2;
% MDS writer period
mdsperiod=0.001;
% MDSWriter segment length
mdsseglen=2000;

startpath=pwd;
fcnpath=fileparts(mfilename('fullpath'));
eval(sprintf('cd %s',fcnpath));

%% MDSParameters stub
fid=fopen('MDSPARAMS1.cfg','w');
obj.printMARTe2parconfig(-1,fid);
fclose(fid);
system('sed -i ''s/obj1.printMARTe2parconfig(-1);//g'' MDSPARAMS1.cfg');
system('sed -i ''s/MDSParameters/MDSParamLoader1/g'' MDSPARAMS1.cfg');

%% MDSWavegen stub
fid=fopen('MDSWAVEGEN1.cfg','w');
wg1dim=obj.printMARTe2wgbusconfig(-1,'algo_demo2_inBus',1000,fid);
fclose(fid);
system('sed -i ''s/obj.printMARTe2wgbusconfig(-1,''algo_demo2_inBus'',1000);//g'' MDSWAVEGEN1.cfg');
system('sed -i ''s/MDSWavegen_algo_demo2_inBus_1/MDSWavegen_1/g'' MDSWAVEGEN1.cfg');
system('sed -i ''s/wavegen_algo_demo2_inBus_1/wavegen_1/g'' MDSWAVEGEN1.cfg');

%% Structured bus stubs

algo_demo2_inbus =Simulink.data.evalinGlobal('algo_demo2','algo_demo2_inBus');
inputbusflat =genbusstructstub('INPUTBUSSTRUCT1.cfg',algo_demo2_inbus,'input','DDB1');

algo_demo2_outbus=Simulink.data.evalinGlobal('algo_demo2','algo_demo2_outBus');
outputbusflat=genbusstructstub('OUTPUTBUSSTRUCT1.cfg',algo_demo2_outbus,'output','DDB1');

%% Wavegen input stubs

genbusflatstub1('INPUTBUSFLAT1.cfg',inputbusflat,'DDB1')

%% StorageBroker output stubs

genbusflatstub1('OUTPUTBUSFLAT1DDB1.cfg',outputbusflat,'DDB1')
genbusflatstub1('OUTPUTBUSFLAT1MDS1.cfg',outputbusflat,'MDSWriter_1')

%% MDSWriter stubs

genbusflatstub2('OUTPUTBUSMDSWRITER1.cfg',outputbusflat,'demo2.outputs','MDSW1PERIOD','MDSW1SEGLEN');

%% cfg build
system('cp algo_demo2.cfgsrc algo_demo2.cfg');
system('sed -i -e "/>MDSPARAMS1</r MDSPARAMS1.cfg" -e "/>MDSPARAMS1</d" algo_demo2.cfg');
system('sed -i -e "/>RTAPPPRESL</r rtapp-presl.cfgsrc" -e "/>RTAPPPRESL</d" algo_demo2.cfg');
system('sed -i -e "/>RTAPPSL</r rtapp-simulinkwrapper.cfgsrc" -e "/>RTAPPSL</d" algo_demo2.cfg');
system('sed -i -e "/>RTAPPPOSTSL</r rtapp-postsl.cfgsrc" -e "/>RTAPPPOSTSL</d" algo_demo2.cfg');
system('sed -i -e "/>DATA</r data.cfgsrc" -e "/>DATA</d" algo_demo2.cfg');
system('sed -i -e "/>STATES</r states.cfgsrc" -e "/>STATES</d" algo_demo2.cfg');
system('sed -i -e "/>INPUTBUSFLAT1</r INPUTBUSFLAT1.cfg" -e "/>INPUTBUSFLAT1</d" algo_demo2.cfg');
system('sed -i -e "/>INPUTBUSSTRUCT1</r INPUTBUSSTRUCT1.cfg" -e "/>INPUTBUSSTRUCT1</d" algo_demo2.cfg');
system('sed -i -e "/>OUTPUTBUSSTRUCT1</r OUTPUTBUSSTRUCT1.cfg" -e "/>OUTPUTBUSSTRUCT1</d" algo_demo2.cfg');
system('sed -i -e "/>OUTPUTBUSFLAT1DDB1</r OUTPUTBUSFLAT1DDB1.cfg" -e "/>OUTPUTBUSFLAT1DDB1</d" algo_demo2.cfg');
system('sed -i -e "/>OUTPUTBUSFLAT1MDS1</r OUTPUTBUSFLAT1MDS1.cfg" -e "/>OUTPUTBUSFLAT1MDS1</d" algo_demo2.cfg');
system('sed -i -e "/>MDSWAVEGEN1</r MDSWAVEGEN1.cfg" -e "/>MDSWAVEGEN1</d" algo_demo2.cfg');
system('sed -i -e "/>OUTPUTBUSMDSWRITER1</r OUTPUTBUSMDSWRITER1.cfg" -e "/>OUTPUTBUSMDSWRITER1</d" algo_demo2.cfg');
system(sprintf('sed -i "s/>WAVEGEN1_ELEMS</%d/g" algo_demo2.cfg',wg1dim));

%% cfg adapt
system(sprintf('sed -i "s/MDSSRCSHOT/%d/g" algo_demo2.cfg',mdssrcshot));
system(sprintf('sed -i "s/MDSDSTSHOT/%d/g" algo_demo2.cfg',mdsdstshot));
system(sprintf('sed -i "s/VERBOSITY/%d/g" algo_demo2.cfg',verbosity));
system(sprintf('sed -i "s/MDSW1PERIOD/%d/g" algo_demo2.cfg',mdsperiod));
system(sprintf('sed -i "s/MDSW1SEGLEN/%d/g" algo_demo2.cfg',mdsseglen));

