function TP = algo_demo2_loadtp()
% Setup tunable control params default values
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

TP.enable           = true;
TP.gain             = single(2);
TP.refmodel.gain    = single(4); % another gain used in referenced model
TP.rowvect          = int32([1 2 3]);
TP.colvect          = single([1 2 3]');
TP.matrix           = int8([1 2; 3 4]);

end
