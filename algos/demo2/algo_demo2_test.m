classdef algo_demo2_test < algo_test
  % test for algo_demo2
  %
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  properties
    algoobj = @algoobj_demo2;
  end
  
end