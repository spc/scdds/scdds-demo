function obj = algoobj_demo2()

%% Demonstration algorithm 2
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
obj = SCDDSdemo_algo('algo_demo2');

%% Timing of the algorithm
obj=obj.settiming(-1,1e-3,1.0);

%% Fixed parameters init functions 
obj=obj.addfpinitfcn('algo_demo2_loadfp','algo_demo2_fp');

%% Tunable parameters structure name
obj=obj.addtunparamstruct('algo_demo2_tp', @()algo_demo2_loadtp(), false);

%% Tunable parameters
% Possible bug with MARTe2 distributed client, see scdds-demo project issue #5
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo2.params.enable'        ,'enable'          ,'mdshelp','a bool param')); 
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo2.params.gain'          ,'gain'            ,'mdshelp','a tunable gain'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo2.params.refmodel.gain' ,'refmodel.gain'   ,'mdshelp','a tunable gain'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo2.params.rowvect'       ,'rowvect'         ,'mdshelp','a row vector'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo2.params.colvect'       ,'colvect'         ,'mdshelp','a column vector'));
obj=obj.addparameter(SCDDSdemo_mdsparnumeric('demo2.params.matrix'        ,'matrix'          ,'mdshelp','a matrix'));

%% Wavegens
mdsconfig = SCDDSdemo_mdsconfig();
obj=obj.addwavegenbasetruct('algo_demo2_inbus1');
obj=obj.addwavegen(SCDclass_mdswgsigsingle( 'demo2.inputs.signal1','signal1'        ,'srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree));
obj=obj.addwavegen(SCDclass_mdswgsigsingle( 'demo2.inputs.signal2','signal2'        ,'srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree));

%% Buses
obj = obj.addbus('algo_demo2_inBus', 'algo_demo2_inBus_def' );
obj = obj.addbus('algo_demo2_outBus', 'algo_demo2_outBus_def' );

 % function handle that returns cell arays of buses and busnames to be registered
obj = obj.addbus('',@() algo_demo2_signal_buses());

%% Tasks

%% Print (optional)
obj.printinfo;

end

