% Bus object: SCDalgo_demo2_inBus 
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'signal1';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'single';
elems(1).SampleTime = -1;
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'signal2';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).SampleTime = -1;
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

ii=3;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal3';
elems(ii).Dimensions = 1;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=4;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal4';
elems(ii).Dimensions = 1;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

ii=5;
elems(ii) = Simulink.BusElement;
elems(ii).Name = 'signal5';
elems(ii).Dimensions = 1;
elems(ii).DimensionsMode = 'Fixed';
elems(ii).DataType = 'single';
elems(ii).SampleTime = -1;
elems(ii).Complexity = 'real';
elems(ii).Min = [];
elems(ii).Max = [];
elems(ii).DocUnits = '';
elems(ii).Description = '';

algo_demo2_inBus = Simulink.Bus;
algo_demo2_inBus.HeaderFile = '';
algo_demo2_inBus.Description = '';
algo_demo2_inBus.DataScope = 'Auto';
algo_demo2_inBus.Alignment = -1;
algo_demo2_inBus.Elements = elems;
clear elems;
