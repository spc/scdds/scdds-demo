## SCDDS DEMO
This is a project to demonstrate the basic capabilities of `SCDDS`.
It includes `scdds-core` as a submodule. See that project for more information.

C. Galperti
F. Felici
EPFL-SPC 2022

## Installation
```
# clone the repository
git clone git@gitlab.epfl.ch:spc/scd/scdds-demos
# init and update submodules
git submodule update --recursive --init
```

## Getting started
Start matlab using
```
./matlabscdds_demo.sh
```
then open `demo_test_main.m` file which contains a run-through of the demonstration

