function tests_matlab(test_case)
% function to call tests from test_script.sh
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
try
   fprintf('\n Running test file: %s\n',mfilename('fullpath'));
   fprintf('     Time: %s\n',datestr(now));
   
   passed = run_scdds_demo_tests(test_case); % add a call to your test script here, with optional test_case input
   exit_code = int32(~passed); % convert to bash shell convention
catch ME
   disp(getReport(ME))
   exit_code = 1;
end
exit(exit_code);
