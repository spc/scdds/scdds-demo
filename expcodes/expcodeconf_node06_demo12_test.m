function [obj] = expcodeconf_node06_demo12_test()

%% EXPcode object
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
obj=SCDDSdemo_expcode('node06_demo_test');

%% algorithms objects
demo1       = algoobj_demo1;
demo2       = algoobj_demo2;

%% wrapper objects
wrapper1 = SCDDSdemo_wrapper('SCDwrap_demo1',1e-3);
wrapper1 = wrapper1.addalgo(demo1);

wrapper2 = SCDDSdemo_wrapper('SCDwrap_demo2',1e-3);
wrapper2 = wrapper2.addalgo(demo2);

%% node objects
node = SCDDSdemo_node(6);
node = node.addwrapper(wrapper1,1,1000);
node = node.addwrapper(wrapper2,2,1000);

%% Setting node into main expcode obj
obj = obj.setnode(node,6);

