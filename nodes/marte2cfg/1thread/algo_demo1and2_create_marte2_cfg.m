% This script creates a testable
% MARTe2 cfg file for the cfs1 + cfs2 algorithms
% with these specifications:
%
% single realtime thread, LinuxTimer synched at 1kHz, 2 SimulinkWrapGAM in
%  sequence
% tunable parameters MDSplus link via MDSObjLoader instantiations
% tunable waveforms  MDSplus link via MDSObjWavegen instantiations
% inter algorithm communication via dedicated proc buses routing 
% algorithms output MDSplus link via standard MDSWriter
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

%% Parameters
% source MDSplus shotnumber
mdssrcshot=-1;
% destination MDSplus shotnumber (-1 stands for increment current by 1)
mdsdstshot=-1;
% verbosity level
verbosity=2;
% MDS writer period
mdsperiod=0.001;

%% Paths handling

startpath=pwd;
fcnpath=fileparts(mfilename('fullpath'));
eval(sprintf('cd %s',fcnpath));

%% MDSParameters stubs generation
fid=fopen('MDSPARAMS1.cfg','w');
obj1.printMARTe2parconfig(-1,fid);
fclose(fid);
system('sed -i ''s/obj1.printMARTe2parconfig(-1);//g'' MDSPARAMS1.cfg');
system('sed -i ''s/MDSParameters/MDSParamLoader1/g'' MDSPARAMS1.cfg');

fid=fopen('MDSPARAMS2.cfg','w');
obj2.printMARTe2parconfig(-1,fid);
fclose(fid);
system('sed -i ''s/obj2.printMARTe2parconfig(-1);//g'' MDSPARAMS2.cfg');
system('sed -i ''s/MDSParameters/MDSParamLoader2/g'' MDSPARAMS2.cfg');

%% MDSWavegen datasource part generation
fid=fopen('MDSWAVEGEN1.cfg','w');
[~,wg1dim]=obj1.printMARTe2wgbusconfig(-1,'algo_cfs1_inBus',1000,fid);
fclose(fid);
system('sed -i ''s/obj1.printMARTe2wgbusconfig(-1,''algo_cfs1_inBus'',1000);//g'' MDSWAVEGEN1.cfg');
system('sed -i ''s/MDSWavegen_algo_cfs1_inBus_1/MDSWavegen_1/g'' MDSWAVEGEN1.cfg');
system('sed -i ''s/wavegen_algo_cfs1_inBus_1/wavegen_1/g'' MDSWAVEGEN1.cfg');

fid=fopen('MDSWAVEGEN2.cfg','w');
[~,wg2dim]=obj2.printMARTe2wgbusconfig(-1,'algo_cfs2_inBus',1000,fid);
fclose(fid);
system('sed -i ''s/obj2.printMARTe2wgbusconfig(-1,''algo_cfs2_inBus'',1000);//g'' MDSWAVEGEN2.cfg');
system('sed -i ''s/MDSWavegen_algo_cfs2_inBus_1/MDSWavegen_2/g'' MDSWAVEGEN2.cfg');
system('sed -i ''s/wavegen_algo_cfs2_inBus_1/wavegen_2/g'' MDSWAVEGEN2.cfg');

%% structured bus stubs

algo_cfs1_inBus=Simulink.data.evalinGlobal('algo_cfs1','algo_cfs1_inBus');
cfs1inputbusflat=genbusstructstub('INPUTBUSSTRUCT1.cfg',algo_cfs1_inBus,'input','DDB1');

algo_cfs1_outBus=Simulink.data.evalinGlobal('algo_cfs1','algo_cfs1_outBus');
cfs1outputbusflat=genbusstructstub('OUTPUTBUSSTRUCT1.cfg',algo_cfs1_outBus,'output','DDB1');

algo_cfs2_inBus=Simulink.data.evalinGlobal('algo_cfs2','algo_cfs2_inBus');
cfs2inputbusflat=genbusstructstub('INPUTBUSSTRUCT2.cfg',algo_cfs2_inBus,'input','DDB2');

algo_cfs2_outBus=Simulink.data.evalinGlobal('algo_cfs2','algo_cfs2_outBus');
cfs2outputbusflat=genbusstructstub('OUTPUTBUSSTRUCT2.cfg',algo_cfs2_outBus,'output','DDB2');

%% Data types stubs

algo_cfs1_procout=Simulink.data.evalinGlobal('algo_cfs1','algo_cfs1_procout');
algo_cfs2_procout=Simulink.data.evalinGlobal('algo_cfs2','algo_cfs2_procout');
%algo_cfs12_procin=Simulink.data.evalinGlobal('algo_cfs1','algo_cfs12_procin');

fid=fopen('TYPEDEF.cfg','w');
bus2marte2type(algo_cfs1_procout, 'algo_cfs1_procout', fid);
bus2marte2type(algo_cfs2_procout, 'algo_cfs2_procout', fid);
%bus2marte2type(algo_cfs12_procin, 'algo_cfs12_procin', fid);
fclose(fid);

%% Clean

clear algo_cfs1_outBus algo_cfs1_outBus algo_cfs1_procout 
clear algo_cfs2_outBus algo_cfs2_outBus algo_cfs2_procout 
clear algo_cfs12_procin

%% flat bus stubs

genbusflatstub1('INPUTBUS1DDB1FLAT.cfg',cfs1inputbusflat,'DDB1');
genbusflatstub1('INPUTBUS2DDB2FLAT.cfg',cfs2inputbusflat,'DDB2');

genbusflatstub1('OUTPUTBUSFLAT1.cfg',cfs1outputbusflat,'DDB1');
genbusflatstub1('OUTPUTBUSFLAT2.cfg',cfs2outputbusflat,'DDB2');

genbusflatstub1('OUTPUTBUSFLATMDS1.cfg',cfs1outputbusflat,'MDSWriter_1');
genbusflatstub1('OUTPUTBUSFLATMDS2.cfg',cfs2outputbusflat,'MDSWriter_2');

genbusflatstub2('OUTPUTBUSMDSWRITER1.cfg',cfs1outputbusflat,'cfs1.outputs','MDSW0PERIOD','2000');
genbusflatstub2('OUTPUTBUSMDSWRITER2.cfg',cfs1outputbusflat,'cfs2.outputs','MDSW0PERIOD','2000');

%% cfg build
system('cp algo_cfs1and2_1thread.cfgsrc algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>MDSPARAMS1</r MDSPARAMS1.cfg" -e "/>MDSPARAMS1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>MDSPARAMS2</r MDSPARAMS2.cfg" -e "/>MDSPARAMS2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>RTAPPPRESL</r rtapp-presl.cfgsrc" -e "/>RTAPPPRESL</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>RTAPPSL</r rtapp-simulinkwrapper.cfgsrc" -e "/>RTAPPSL</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>RTAPPPOSTSL</r rtapp-postsl.cfgsrc" -e "/>RTAPPPOSTSL</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>DATA</r data.cfgsrc" -e "/>DATA</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>STATES</r states.cfgsrc" -e "/>STATES</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>INPUTBUS1DDB1FLAT</r INPUTBUS1DDB1FLAT.cfg" -e "/>INPUTBUS1DDB1FLAT</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>INPUTBUS2DDB2FLAT</r INPUTBUS2DDB2FLAT.cfg" -e "/>INPUTBUS2DDB2FLAT</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>INPUTBUSSTRUCT1</r INPUTBUSSTRUCT1.cfg" -e "/>INPUTBUSSTRUCT1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>INPUTBUSSTRUCT2</r INPUTBUSSTRUCT2.cfg" -e "/>INPUTBUSSTRUCT2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>OUTPUTBUSSTRUCT1</r OUTPUTBUSSTRUCT1.cfg" -e "/>OUTPUTBUSSTRUCT1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>OUTPUTBUSSTRUCT2</r OUTPUTBUSSTRUCT2.cfg" -e "/>OUTPUTBUSSTRUCT2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>OUTPUTBUSFLAT</r storagebroker1.cfg" -e "/>OUTPUTBUSFLAT</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>OUTPUTBUSFLATMDS</r storagebroker2.cfg" -e "/>OUTPUTBUSFLATMDS</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>MDSWAVEGEN1</r MDSWAVEGEN1.cfg" -e "/>MDSWAVEGEN1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>MDSWAVEGEN2</r MDSWAVEGEN2.cfg" -e "/>MDSWAVEGEN2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>OUTPUTBUSFLAT1</r OUTPUTBUSFLAT1.cfg" -e "/>OUTPUTBUSFLAT1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>OUTPUTBUSFLAT2</r OUTPUTBUSFLAT2.cfg" -e "/>OUTPUTBUSFLAT2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>OUTPUTBUSFLATMDS1</r OUTPUTBUSFLATMDS1.cfg" -e "/>OUTPUTBUSFLATMDS1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>OUTPUTBUSFLATMDS2</r OUTPUTBUSFLATMDS2.cfg" -e "/>OUTPUTBUSFLATMDS2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>OUTPUTBUSMDSWRITER1</r OUTPUTBUSMDSWRITER1.cfg" -e "/>OUTPUTBUSMDSWRITER1</d" algo_cfs1and2_1thread.cfg');
system('sed -i -e "/>OUTPUTBUSMDSWRITER2</r OUTPUTBUSMDSWRITER2.cfg" -e "/>OUTPUTBUSMDSWRITER2</d" algo_cfs1and2_1thread.cfg');

system('sed -i -e "/>TYPEDEF</r TYPEDEF.cfg" -e "/>TYPEDEF</d" algo_cfs1and2_1thread.cfg');

system(sprintf('sed -i "s/>WAVEGEN1_ELEMS</%d/g" algo_cfs1and2_1thread.cfg',wg1dim));
system(sprintf('sed -i "s/>WAVEGEN2_ELEMS</%d/g" algo_cfs1and2_1thread.cfg',wg2dim));

%% cfg adapt
system(sprintf('sed -i "s/MDSSRCSHOT/%d/g" algo_cfs1and2_1thread.cfg',mdssrcshot));
system(sprintf('sed -i "s/MDSDSTSHOT/%d/g" algo_cfs1and2_1thread.cfg',mdsdstshot));
system(sprintf('sed -i "s/VERBOSITY/%d/g" algo_cfs1and2_1thread.cfg',verbosity));
system(sprintf('sed -i "s/MDSW0PERIOD/%d/g" algo_cfs1and2_1thread.cfg',mdsperiod));

