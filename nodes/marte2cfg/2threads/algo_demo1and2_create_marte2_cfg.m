% This script creates a testable
% MARTe2 cfg file for the demo1 + demo2 algorithms
% with these specifications:
%
% 2 rt threads, 1 SimulinkWrapperGAMs per thread
% tunable parameters MDSplus link via MDSObjLoader instantiations
% tunable waveforms  MDSplus link via MDSObjWavegen instantiations
% inter algorithm communication via dedicated proc buses routing 
% algorithms output MDSplus link via standard MDSWriter
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

%% Parameters
% source MDSplus shotnumber
mdssrcshot=-1;
% destination MDSplus shotnumber (-1 stands for increment current by 1)
mdsdstshot=-1;
% verbosity level
verbosity=2;
% MDS writer period
mdsperiod=0.001;
% Simulink .so location
if isequal(SCDDSdemo_env.getsite(),'EPFL')
    demo1solocation='\"\/home\/tcv_oper\/martecfgs\/tests\/SCDwrap_demo1.so\"';
    demo2solocation='\"\/home\/tcv_oper\/martecfgs\/tests\/SCDwrap_demo2.so\"';    
elseif isequal(SCDDSdemo_env.getsite(),'PSFC')
    demo1solocation='\"\/home\/cfsusertbc\/MARTe2\/MARTe2-examples\/SCDwrap_demo1.so\"';
    demo2solocation='\"\/home\/cfsusertbc\/MARTe2\/MARTe2-examples\/SCDwrap_demo2.so\"';        
end

%% Paths handling
startpath=pwd;
fcnpath=fileparts(mfilename('fullpath'));
eval(sprintf('cd %s',fcnpath));

%% algos obj rerieval
obj1=obj.algos{1};
obj2=obj.algos{2};

%% MDSParameters stubs generation
fid=fopen('MDSPARAMS1.cfg','w');
obj1.printMARTe2parconfig(-1,fid);
fclose(fid);
system('sed -i ''s/obj1.printMARTe2parconfig(-1);//g'' MDSPARAMS1.cfg');
system('sed -i ''s/MDSParameters/MDSParamLoader1/g'' MDSPARAMS1.cfg');

fid=fopen('MDSPARAMS2.cfg','w');
obj2.printMARTe2parconfig(-1,fid);
fclose(fid);
system('sed -i ''s/obj2.printMARTe2parconfig(-1);//g'' MDSPARAMS2.cfg');
system('sed -i ''s/MDSParameters/MDSParamLoader2/g'' MDSPARAMS2.cfg');

%% MDSWavegen datasource part generation
fid=fopen('MDSWAVEGEN1.cfg','w');
[~,wg1dim]=obj1.printMARTe2wgbusconfig(-1,'WG0601bus',1000,fid,'ddname','SCDwrap_demo1.sldd');
fclose(fid);
system('sed -i ''s/MDSWavegen_WG0601bus_1/MDSWavegen_1/g'' MDSWAVEGEN1.cfg');
system('sed -i ''s/wavegen_WG0601bus_1/wavegen_1/g'' MDSWAVEGEN1.cfg');

fid=fopen('MDSWAVEGEN2.cfg','w');
[~,wg2dim]=obj2.printMARTe2wgbusconfig(-1,'WG0602bus',1000,fid,'ddname','SCDwrap_demo2.sldd');
fclose(fid);
system('sed -i ''s/MDSWavegen_WG0602bus_1/MDSWavegen_2/g'' MDSWAVEGEN2.cfg');
system('sed -i ''s/wavegen_WG0602bus_1/wavegen_2/g'' MDSWAVEGEN2.cfg');

% TODO: this is a workaround,
% we need to solve printMARTe2wgbusconfig same name
% issue at the root
%system('sed -i ''s/demo1/demo2/g'' MDSWAVEGEN2.cfg');

%% structured bus stubs

WG0601bus=Simulink.data.evalinGlobal('demomain','WG0601bus');
WG0601busflat=genbusstructstub('INPUTBUSSTRUCT1.cfg',WG0601bus,'wavegen','DDB1');

algo_demo1_outBus=Simulink.data.evalinGlobal('demomain','algo_demo1_outBus');
mdsout01=Simulink.data.evalinGlobal('demomain','mdsout01');
mdsout01busflat=genbusstructstub('OUTPUTBUSSTRUCT1.cfg',mdsout01,'mdsout','DDB1');

vecmatinbus=Simulink.data.evalinGlobal('demomain','vecmatin');
vecmatinbusflat=genbusstructstub('INPUTVECMAT1.cfg',vecmatinbus,'vecmatin','DDB1');

WG0602bus=Simulink.data.evalinGlobal('demomain','WG0602bus');
WG0602busflat=genbusstructstub('INPUTBUSSTRUCT2.cfg',WG0602bus,'wavegen','DDB2');

algo_demo2_outBus=Simulink.data.evalinGlobal('demomain','algo_demo2_outBus');
mdsout02=Simulink.data.evalinGlobal('demomain','mdsout02');
mdsout02busflat=genbusstructstub('OUTPUTBUSSTRUCT2.cfg',mdsout02,'mdsout','DDB2');

%% Data types stubs

procout01=Simulink.data.evalinGlobal('demomain','procout01');
procout02=Simulink.data.evalinGlobal('demomain','procout02');
procin   =Simulink.data.evalinGlobal('demomain','procin');

fid=fopen('TYPEDEF.cfg','w');
bus2marte2type(procout01, 'procout01type', fid);
bus2marte2type(procout02, 'procout02type', fid);
bus2marte2type(procin,    'procintype', fid);
fclose(fid);

%% Clean

clear algo_main1_outBus algo_main1_outBus algo_main1_procout 
clear algo_main2_outBus algo_main2_outBus algo_main2_procout 
clear algo_main12_procin

%% flat bus stubs

genbusflatstub1('INPUTBUS1DDB1FLAT.cfg',WG0601busflat,'DDB1',true);
genbusflatstub1('INPUTBUS2DDB2FLAT.cfg',WG0602busflat,'DDB2');

genbusflatstub1('OUTPUTBUSFLAT1.cfg',mdsout01busflat,'DDB1');
genbusflatstub1('OUTPUTBUSFLAT2.cfg',mdsout02busflat,'DDB2');

genbusflatstub1('OUTPUTBUSFLATMDS1.cfg',mdsout01busflat,'MDSWriter_1',true);
genbusflatstub1('OUTPUTBUSFLATMDS2.cfg',mdsout02busflat,'MDSWriter_2');

genbusflatstub2('OUTPUTBUSMDSWRITER1.cfg',mdsout01busflat,'demo1.outputs.demo1','MDSW0PERIOD','2000');
genbusflatstub2('OUTPUTBUSMDSWRITER2.cfg',mdsout02busflat,'demo2.outputs.demo2','MDSW0PERIOD','2000');

%% cfg build
system('cp algo_demo1and2_2threads.cfgsrc algo_demo1and2_2threads.cfg');
system('sed -i -e "/>MDSPARAMS1</r MDSPARAMS1.cfg" -e "/>MDSPARAMS1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>MDSPARAMS2</r MDSPARAMS2.cfg" -e "/>MDSPARAMS2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>RTAPPTHREAD1</r rtapp-thread1.cfgsrc" -e "/>RTAPPTHREAD1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>RTAPPTHREAD2</r rtapp-thread2.cfgsrc" -e "/>RTAPPTHREAD2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>DATA</r data.cfgsrc" -e "/>DATA</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>STATES</r states.cfgsrc" -e "/>STATES</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>INPUTBUS1DDB1FLAT</r INPUTBUS1DDB1FLAT.cfg" -e "/>INPUTBUS1DDB1FLAT</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>INPUTBUS2DDB2FLAT</r INPUTBUS2DDB2FLAT.cfg" -e "/>INPUTBUS2DDB2FLAT</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>INPUTBUSSTRUCT1</r INPUTBUSSTRUCT1.cfg" -e "/>INPUTBUSSTRUCT1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>INPUTBUSSTRUCT2</r INPUTBUSSTRUCT2.cfg" -e "/>INPUTBUSSTRUCT2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>INPUTVECMAT1</r INPUTVECMAT1.cfg" -e "/>INPUTVECMAT1</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>OUTPUTBUSSTRUCT1</r OUTPUTBUSSTRUCT1.cfg" -e "/>OUTPUTBUSSTRUCT1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>OUTPUTBUSSTRUCT2</r OUTPUTBUSSTRUCT2.cfg" -e "/>OUTPUTBUSSTRUCT2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>OUTPUTBUSFLAT</r storagebroker1.cfg" -e "/>OUTPUTBUSFLAT</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>OUTPUTBUSFLATMDS</r storagebroker2.cfg" -e "/>OUTPUTBUSFLATMDS</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>MDSWAVEGEN1</r MDSWAVEGEN1.cfg" -e "/>MDSWAVEGEN1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>MDSWAVEGEN2</r MDSWAVEGEN2.cfg" -e "/>MDSWAVEGEN2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>OUTPUTBUSFLAT1</r OUTPUTBUSFLAT1.cfg" -e "/>OUTPUTBUSFLAT1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>OUTPUTBUSFLAT2</r OUTPUTBUSFLAT2.cfg" -e "/>OUTPUTBUSFLAT2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>OUTPUTBUSFLATMDS1</r OUTPUTBUSFLATMDS1.cfg" -e "/>OUTPUTBUSFLATMDS1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>OUTPUTBUSFLATMDS2</r OUTPUTBUSFLATMDS2.cfg" -e "/>OUTPUTBUSFLATMDS2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>OUTPUTBUSMDSWRITER1</r OUTPUTBUSMDSWRITER1.cfg" -e "/>OUTPUTBUSMDSWRITER1</d" algo_demo1and2_2threads.cfg');
system('sed -i -e "/>OUTPUTBUSMDSWRITER2</r OUTPUTBUSMDSWRITER2.cfg" -e "/>OUTPUTBUSMDSWRITER2</d" algo_demo1and2_2threads.cfg');

system('sed -i -e "/>TYPEDEF</r TYPEDEF.cfg" -e "/>TYPEDEF</d" algo_demo1and2_2threads.cfg');

system(sprintf('sed -i "s/>WAVEGEN1_ELEMS</%d/g" algo_demo1and2_2threads.cfg',wg1dim));
system(sprintf('sed -i "s/>WAVEGEN2_ELEMS</%d/g" algo_demo1and2_2threads.cfg',wg2dim));
system(sprintf('sed -i "s/WAVEGEN_WG0601BUS_1_SIZE/%d/g" algo_demo1and2_2threads.cfg',wg1dim));
system(sprintf('sed -i "s/WAVEGEN_WG0602BUS_1_SIZE/%d/g" algo_demo1and2_2threads.cfg',wg2dim));

%% cfg adapt
system(sprintf('sed -i "s/MDSSRCSHOT/%d/g" algo_demo1and2_2threads.cfg',mdssrcshot));
system(sprintf('sed -i "s/MDSDSTSHOT/%d/g" algo_demo1and2_2threads.cfg',mdsdstshot));
system(sprintf('sed -i "s/VERBOSITY/%d/g" algo_demo1and2_2threads.cfg',verbosity));
system(sprintf('sed -i "s/MDSW0PERIOD/%d/g" algo_demo1and2_2threads.cfg',mdsperiod));
system(sprintf('sed -i "s/>DEMOSO1LOCATION</%s/g" algo_demo1and2_2threads.cfg',demo1solocation));
system(sprintf('sed -i "s/>DEMOSO2LOCATION</%s/g" algo_demo1and2_2threads.cfg',demo2solocation));

