function [res] = tdi(node)
% tmp tdi function - hack to be removed
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
res=timeseries;
res.Time=mdsvalue(['dim_of(' node ')']);
res.Data=mdsvalue(node)';
end