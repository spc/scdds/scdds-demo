% This script will crate automatically
% the TCL files for building 
% parameters, inputs and outputs trees, using SPC mds-matlab-structs
% functions (liked in a git submodule)
%
% it is assumed that the main algo_demo1_tp
% structure is already present in the base workspace
%
% if successfully executed the three generated TCL scripts:
% /tmp/<username>/demo1_tp.tcl
% /tmp/<username>/demo1_inbus.tcl
% /tmp/<username>/demo1_outbus.tcl
% can be executed by a MDSplus tree edit enabled user via:
% cat /tmp/<username>/demo1_tp.tcl | mdstcl
% and so on
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

addpath ../../core/mds/mds-matlab-structs

%% tunable parameters
tpsrc=Simulink.data.evalinGlobal('demomain','algo_demo1_tp_tmpl').Value;
tpsrcpad=struct();
tpsrcpad.demo1.params=tpsrc;
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(tpsrcpad,'demo1_tp.tcl','scdds','value')
clear tpsrc tpsrcpad

%% input bus
inbus=Simulink.data.evalinGlobal('demomain','WG0601bus');
inbussrc=Simulink.Bus.createMATLABStruct('inbus');
inbussrcpad.demo1.inputs=inbussrc;
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(inbussrcpad,'demo1_inbus.tcl','scdds','data','full_name','dim','signal')
clear inbus inbussrc inbussrcpad

%% output bus
algo_demo1_outBus=Simulink.data.evalinGlobal('demomain','algo_demo1_outBus');
outbus=Simulink.data.evalinGlobal('demomain','mdsout01');
outbussrc=Simulink.Bus.createMATLABStruct('outbus');
outbussrcpad.demo1.outputs=outbussrc;
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(outbussrcpad,'demo1_outbus.tcl','scdds','data','full_name','dim','signal')
clear outbus outbussrc outbussrcpad

%% metadata channels
metach.demo1.system.ch01=single(0);
metach.demo1.system.ch02=single(0);
metach.demo1.system.ch03=single(0);
metach.demo1.system.ch04=single(0);
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(metach,'demo1_system.tcl','scdds','data','full_name','dim','signal')
clear metach

