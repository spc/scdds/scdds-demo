function scdds_demo_paths()
% Script to add SCDDS demos
%
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

pause(10);
vv=ver('matlab'); 
assert(~verLessThan('matlab','9.6'),'This demo will not work for matlab<9.6');

disp('Adding SCDDS DEMO project paths');
thispath = fileparts(mfilename('fullpath' ));
addpath(genpath(fullfile(thispath,'algos')));
addpath(genpath(fullfile(thispath,'nodes')));
addpath(genpath(fullfile(thispath,'src'  )));
addpath(genpath(fullfile(thispath,'expcodes'  )));
addpath(genpath(fullfile(thispath,'tests')));
addpath(thispath);
corepath = fullfile(thispath,'core'  );
run(fullfile(corepath,'scdds_core_paths'));

%% Set paths for generated code
% set code generation and cache file location
gencodes = fullfile(fileparts(mfilename('fullpath')),'gencodes');
assert(logical(exist(gencodes,'dir')),'folder %s does not exist',gencodes)
fprintf('setting Simulink Cache and CodeGen folders for SCDDSS demo in %s\n',gencodes)
CacheFolder   = fullfile(gencodes,'CacheFolder');
CodeGenFolder = fullfile(gencodes,'CodeGenFolder');
pause(2); % keep this here because matlab is black magic
Simulink.fileGenControl('set',...
  'CacheFolder',CacheFolder,...
  'CodeGenFolder',CodeGenFolder,...
  'createdir',true);

end
