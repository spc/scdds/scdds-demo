classdef (Abstract) algo_test < SCDDSalgo_test & SCDDSdemo_tests
  % Derive from SCDDS core and common tests
  %
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
end