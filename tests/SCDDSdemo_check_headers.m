classdef SCDDSdemo_check_headers < check_headers
  % Checks whether desired copyright headers are in place
  % Can be used to fix them automatically by setting `dofix=true`
  % mytest = check_headers
  % mytest.dofix = true; % changes property
  % mytest.run; % runs
  %
  % [SCDDS - Simulink Control System Development and Deployment Suite]
  
  properties
    dofix = false; % if true, modify files to fix header
    IgnoredPaths = {'slprj','.git','core',mfilename};
    old_headers    = {'SCDDS - Simulink Control','GNU Lesser'}
    desired_header = {'[ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.',...
      'Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.'}
     test_folder = fileparts(fileparts(mfilename('fullpath')));
  end

  methods(Test,TestTags={'unit'})
    function check_and_fix_files(testCase)
      check_and_fix_files@check_headers(testCase);
    end
  end
end
