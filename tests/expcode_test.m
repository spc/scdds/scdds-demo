classdef expcode_test < SCDDSexpcodes_test & SCDDSdemo_tests

  methods
    function obj=expcode_test
      % constructor
      %
      %
      % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
      % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
      obj@SCDDSexpcodes_test;
      obj.expcode_obj = expcodeconf_node06_demo12_test();
      obj.shot = -1; % shot for actualizing
    end
  end
end